function tabHandler(tabs) {
	chrome.tabs.sendMessage(tabs[0].id, 
		{action: "extractList"}, 
		function(response){});
}

chrome.commands.onCommand.addListener(function(command) {
	if (command == "extractList") {
		chrome.tabs.query({active: true, currentWindow: true}, tabHandler);
	}
});

