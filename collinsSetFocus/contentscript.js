function setFocus() { 
	var editBox = document.getElementById("search-french-english");
	if (editBox == null)
	editBox = document.getElementById("search-english-french");
	editBox.focus(); 
	// console.log("set focus"); 
}

function checkFrench() {
	document.getElementById("to-english").checked=true;
}

function pageSet() {
	checkFrench();
	setFocus(); 
	window.scrollTo(0,0);
}

pageSet();
chrome.extension.onMessage.addListener(function(msg, sender, sendResponse) {
	// console.log("message received");
	if (msg.action == 'set_focus') { 
		pageSet();
	} 
});
